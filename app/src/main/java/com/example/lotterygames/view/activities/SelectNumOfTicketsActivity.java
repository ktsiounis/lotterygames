package com.example.lotterygames.view.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.lotterygames.R;
import com.example.lotterygames.models.GameDefinition;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectNumOfTicketsActivity extends AppCompatActivity implements NumberPicker.OnValueChangeListener {

    @BindView(R.id.gameSelectedTextView)
    TextView gameSelectedTextView;
    @BindView(R.id.numOfTicketsBtn)
    Button numOfTicketsBtn;
    @BindView(R.id.randomNumsBtn)
    Button randomNumsBtn;

    private GameDefinition gameDefinition;
    private int numOfTickets = 1;
    private String numOfTicketsText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_num_of_tickets);

        ButterKnife.bind(this);

        gameDefinition = getIntent().getParcelableExtra("gameDefinition");

        if (Locale.getDefault().getLanguage().equals("el")) {
            gameSelectedTextView.setText(gameDefinition.getM_NameGr());
        } else {
            gameSelectedTextView.setText(gameDefinition.getM_NameEn());
        }

        numOfTicketsText = getResources().getText(R.string.num_of_tickets).toString();
        numOfTicketsText = numOfTicketsText + numOfTickets;

        numOfTicketsBtn.setText(numOfTicketsText);

        numOfTicketsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNumPicker();
            }
        });

        randomNumsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), RandomNumPresActivity.class);
                i.putExtra("gameDefinition", gameDefinition);
                i.putExtra("numOfTickets", numOfTickets);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
            }
        });

    }

    private void showNumPicker() {
        final Dialog dialog = new Dialog(SelectNumOfTicketsActivity.this);
        dialog.setTitle(R.string.select_tickets);
        dialog.setContentView(R.layout.num_picker_dialog);
        Button okBtn = dialog.findViewById(R.id.button);
        final NumberPicker np = dialog.findViewById(R.id.numberPicker1);
        np.setMaxValue(20);
        np.setMinValue(1);
        np.setValue(numOfTickets);
        np.setOnValueChangedListener(this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                numOfTickets = np.getValue();
            }
        });
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        numOfTickets = newVal;
        numOfTicketsText = getResources().getText(R.string.num_of_tickets).toString();
        numOfTicketsText = numOfTicketsText + numOfTickets;

        numOfTicketsBtn.setText(numOfTicketsText);
    }
}

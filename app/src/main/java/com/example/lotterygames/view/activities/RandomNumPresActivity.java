package com.example.lotterygames.view.activities;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.example.lotterygames.R;
import com.example.lotterygames.adapters.RandomNumsPresAdapter;
import com.example.lotterygames.models.GameDefinition;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RandomNumPresActivity extends AppCompatActivity implements RandomNumsPresAdapter.ItemClickListener {

    @BindView(R.id.randomNumsPresRV)
    RecyclerView randomNumsPresRV;

    ArrayList<String> results = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_num_pres);

        ButterKnife.bind(this);

        GameDefinition gameDefinition = getIntent().getParcelableExtra("gameDefinition");
        int numOfTickets = getIntent().getIntExtra("numOfTickets",0);

        for (int i=0; i<numOfTickets; i++) {
            StringBuilder ticketResults = new StringBuilder();
            for (int j=0; j<gameDefinition.getM_AreaDefsArray().size(); j++) {
                ArrayList<Integer> nums = new ArrayList<>();
                for (int k=0; k<gameDefinition.getM_AreaDefsArray().get(j).getM_NumbersToSelect(); k++) {
                    nums.add((new Random().nextInt(gameDefinition.getM_AreaDefsArray().get(j).getM_MaxNumbers())+1));
                }
                Log.d("Random", "onCreate: " + TextUtils.join(", ", nums));
                ticketResults.append("[").append(TextUtils.join(", ", nums)).append("]");
            }
            results.add(ticketResults.toString());
        }

        RandomNumsPresAdapter adapter = new RandomNumsPresAdapter(this);
        randomNumsPresRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter.setData(results);
        randomNumsPresRV.setAdapter(adapter);

    }

    @Override
    public void onItemClickListener(int position) {
        String res = "";
        int odds = 0;
        int even = 0;
        if (results.get(position).contains("][")) {
            res = results.get(position).replace("][", ", ");
        } else {
            res = results.get(position);
        }
        String[] nums = res.split(", ");
        int sum = 0;
        for (String num : nums) {
            Log.d("Random", "onItemClickListener: " + num);
            num = num.replace("[", "");
            num = num.replace("]", "");
            int numAsInteger = Integer.valueOf(num);
            sum += numAsInteger;
            if (numAsInteger % 2 != 0) {
                odds++;
            } else {
                even++;
            }
        }
        final Dialog dialog = new Dialog(RandomNumPresActivity.this);
        dialog.setTitle(R.string.results);
        dialog.setContentView(R.layout.dialog_results);
        TextView sumTv = dialog.findViewById(R.id.sumResultTv);
        TextView oddsTv = dialog.findViewById(R.id.oddsResultTv);
        TextView evenTv = dialog.findViewById(R.id.evensResultTv);
        sumTv.setText(String.valueOf(sum));
        oddsTv.setText(String.valueOf(odds));
        evenTv.setText(String.valueOf(even));
        dialog.show();
    }
}

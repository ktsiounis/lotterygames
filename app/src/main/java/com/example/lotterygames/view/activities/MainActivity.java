package com.example.lotterygames.view.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.lotterygames.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.title)
    TextView titleTextView;
    @BindView(R.id.costEstimationBtn)
    ImageButton costEstimationBtn;
    @BindView(R.id.pickRandomNumsBtn)
    ImageButton pickRandomNumsBtn;
    @BindView(R.id.randomNumsTextView)
    TextView randomNumsTextView;
    @BindView(R.id.costEstimationTextView)
    TextView costEstimationTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        titleTextView.setText(R.string.first_screen_title_text);
        costEstimationTextView.setText(R.string.cost_estimation);
        randomNumsTextView.setText(R.string.pick_random_nums);

        pickRandomNumsBtn.setImageResource(R.drawable.image_btn_src);
        costEstimationBtn.setImageResource(R.drawable.image_btn_src);

        pickRandomNumsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), SelectLottoActivity.class);
                startActivity(i);
            }
        });


    }
}

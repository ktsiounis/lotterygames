package com.example.lotterygames.view.activities;

import android.content.Intent;
import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lotterygames.R;
import com.example.lotterygames.adapters.LottosListAdapter;
import com.example.lotterygames.models.GameDefinition;
import com.example.lotterygames.models.GameDefinitionsSingleton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectLottoActivity extends AppCompatActivity implements LottosListAdapter.ItemClickListener {

    @BindView(R.id.lottosListView)
    RecyclerView lottosListView;
    private ArrayList<GameDefinition> m_GameDefinitions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_lotto);

        ButterKnife.bind(this);

        GameDefinitionsSingleton gameDefinitionsSingleton = GameDefinitionsSingleton.getInstance();

        m_GameDefinitions = gameDefinitionsSingleton.getM_GameDefinitions();

        for (GameDefinition gameDefinition : m_GameDefinitions) {
            Log.d("MainActivity", "onCreate: " + gameDefinition.getM_GameCode());
        }

        LottosListAdapter adapter = new LottosListAdapter(this);
        lottosListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter.setData(m_GameDefinitions);
        adapter.notifyDataSetChanged();

        lottosListView.setAdapter(adapter);

    }

    @Override
    public void onItemClickListener(int position) {
        Intent i = new Intent(this, SelectNumOfTicketsActivity.class);
        i.putExtra("gameDefinition", m_GameDefinitions.get(position));
        startActivity(i);
    }
}

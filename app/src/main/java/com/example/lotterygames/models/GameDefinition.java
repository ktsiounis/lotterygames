package com.example.lotterygames.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GameDefinition implements Parcelable {

    private int m_GameCode; //The unique game id
    private String m_NameEn; //The English name of the game
    private String m_NameGr; //The Greek name of the game
    private ArrayList<AreaDefinition> m_AreaDefsArray; //An array of AreaDefinition objects

    public GameDefinition(int m_GameCode, String m_NameEn, String m_NameGr, ArrayList<AreaDefinition> m_AreaDefsArray) {
        this.m_GameCode = m_GameCode;
        this.m_NameEn = m_NameEn;
        this.m_NameGr = m_NameGr;
        this.m_AreaDefsArray = m_AreaDefsArray;
    }

    protected GameDefinition(Parcel in) {
        m_GameCode = in.readInt();
        m_NameEn = in.readString();
        m_NameGr = in.readString();
        m_AreaDefsArray = in.createTypedArrayList(AreaDefinition.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(m_GameCode);
        dest.writeString(m_NameEn);
        dest.writeString(m_NameGr);
        dest.writeTypedList(m_AreaDefsArray);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GameDefinition> CREATOR = new Creator<GameDefinition>() {
        @Override
        public GameDefinition createFromParcel(Parcel in) {
            return new GameDefinition(in);
        }

        @Override
        public GameDefinition[] newArray(int size) {
            return new GameDefinition[size];
        }
    };

    public int getM_GameCode() {
        return m_GameCode;
    }

    public void setM_GameCode(int m_GameCode) {
        this.m_GameCode = m_GameCode;
    }

    public String getM_NameEn() {
        return m_NameEn;
    }

    public void setM_NameEn(String m_NameEn) {
        this.m_NameEn = m_NameEn;
    }

    public String getM_NameGr() {
        return m_NameGr;
    }

    public void setM_NameGr(String m_NameGr) {
        this.m_NameGr = m_NameGr;
    }

    public ArrayList<AreaDefinition> getM_AreaDefsArray() {
        return m_AreaDefsArray;
    }

    public void setM_AreaDefsArray(ArrayList<AreaDefinition> m_AreaDefsArray) {
        this.m_AreaDefsArray = m_AreaDefsArray;
    }
}

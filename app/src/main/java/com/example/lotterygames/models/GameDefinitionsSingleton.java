package com.example.lotterygames.models;

import java.util.ArrayList;

public class GameDefinitionsSingleton {

    private static GameDefinitionsSingleton gameDifinitionsInstance = new GameDefinitionsSingleton();
    private ArrayList<GameDefinition> m_GameDefinitions;

    //private constructor.
    private GameDefinitionsSingleton(){
        ArrayList<AreaDefinition> areaDefinitions = new ArrayList<>();
        areaDefinitions.add(new AreaDefinition(5, 45));
        areaDefinitions.add(new AreaDefinition(1, 20));
        m_GameDefinitions = new ArrayList<>();
        m_GameDefinitions.add(new GameDefinition(5000, "Tzoker", "Τζοκερ", areaDefinitions));
        ArrayList<AreaDefinition> areaDefinitions2 = new ArrayList<>();
        areaDefinitions2.add(new AreaDefinition(6, 49));
        m_GameDefinitions.add(new GameDefinition(5001, "Lotto", "Λοττο", areaDefinitions2));
    }

    public static GameDefinitionsSingleton getInstance() {
        return gameDifinitionsInstance;
    }

    public ArrayList<GameDefinition> getM_GameDefinitions() {
        return m_GameDefinitions;
    }
}

package com.example.lotterygames.models;

import android.os.Parcel;
import android.os.Parcelable;

public class AreaDefinition implements Parcelable {

    private int m_NumbersToSelect;
    private int m_MaxNumbers;

    public AreaDefinition(int m_NumbersToSelect, int m_MaxNumbers) {
        this.m_NumbersToSelect = m_NumbersToSelect;
        this.m_MaxNumbers = m_MaxNumbers;
    }

    protected AreaDefinition(Parcel in) {
        m_NumbersToSelect = in.readInt();
        m_MaxNumbers = in.readInt();
    }

    public static final Creator<AreaDefinition> CREATOR = new Creator<AreaDefinition>() {
        @Override
        public AreaDefinition createFromParcel(Parcel in) {
            return new AreaDefinition(in);
        }

        @Override
        public AreaDefinition[] newArray(int size) {
            return new AreaDefinition[size];
        }
    };

    public int getM_NumbersToSelect() {
        return m_NumbersToSelect;
    }

    public void setM_NumbersToSelect(int m_NumbersToSelect) {
        this.m_NumbersToSelect = m_NumbersToSelect;
    }

    public int getM_MaxNumbers() {
        return m_MaxNumbers;
    }

    public void setM_MaxNumbers(int m_MaxNumbers) {
        this.m_MaxNumbers = m_MaxNumbers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(m_NumbersToSelect);
        dest.writeInt(m_MaxNumbers);
    }
}

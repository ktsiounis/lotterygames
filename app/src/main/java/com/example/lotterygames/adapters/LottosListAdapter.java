package com.example.lotterygames.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lotterygames.R;
import com.example.lotterygames.models.GameDefinition;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LottosListAdapter extends RecyclerView.Adapter<LottosListAdapter.ViewHolder> {

    ArrayList<GameDefinition> gameDefinitions = new ArrayList<>();
    private LottosListAdapter.ItemClickListener mOnClickListener;

    public LottosListAdapter(LottosListAdapter.ItemClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View card = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lottos_list_item, viewGroup,false);
        return new ViewHolder(card);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (Locale.getDefault().getLanguage().equals("el")) {
            viewHolder.lottoName.setText(gameDefinitions.get(i).getM_NameGr());
        } else {
            viewHolder.lottoName.setText(gameDefinitions.get(i).getM_NameEn());
        }
    }

    @Override
    public int getItemCount() {
        return gameDefinitions.size();
    }

    public void setData(ArrayList<GameDefinition> gameDefinitions) {
        if(this.gameDefinitions != null){
            this.gameDefinitions.clear();
            this.gameDefinitions.addAll(gameDefinitions);
        } else {
            this.gameDefinitions = gameDefinitions;
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.lottoNameTextView)
        TextView lottoName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mOnClickListener.onItemClickListener(position);
        }
    }

    public interface ItemClickListener {
        void onItemClickListener(int position);
    }
}

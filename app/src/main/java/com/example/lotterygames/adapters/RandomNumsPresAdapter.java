package com.example.lotterygames.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lotterygames.R;
import com.example.lotterygames.view.activities.RandomNumPresActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RandomNumsPresAdapter extends RecyclerView.Adapter<RandomNumsPresAdapter.ViewHolder> {

    private ArrayList<String> results = new ArrayList<>();
    private RandomNumsPresAdapter.ItemClickListener mOnClickListener;

    public RandomNumsPresAdapter(ItemClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View card = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lottos_list_item, viewGroup,false);
        return new ViewHolder(card);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.result.setText(results.get(i));
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public void setData(ArrayList<String> results) {
        if(this.results != null){
            this.results.clear();
            this.results.addAll(results);
        } else {
            this.results = results;
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.lottoNameTextView)
        TextView result;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mOnClickListener.onItemClickListener(position);
        }
    }

    public interface ItemClickListener {
        void onItemClickListener(int position);
    }

}
